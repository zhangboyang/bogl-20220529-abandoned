From e7fe989fbbda4acfd9971604b7ffa84899cb343a Mon Sep 17 00:00:00 2001
From: Zhang Boyang <zhangboyang.id@gmail.com>
Date: Mon, 23 May 2022 22:07:37 +0800
Subject: [PATCH v2 4/8] Use ioctl(FBIOPAN_DISPLAY) to update screen after
 drawing

Some framebuffer driver like i915 need explicit notify after drawing,
otherwise modifications in graphics buffer will not be reflected on
screen, so use FBIOPAN_DISPLAY to do this notify.
---
 bogl-term.c |  1 +
 bogl.c      | 19 ++++++++++++++++++-
 bogl.h      |  1 +
 3 files changed, 20 insertions(+), 1 deletion(-)

diff --git a/bogl-term.c b/bogl-term.c
index d5780d3..1f68523 100644
--- a/bogl-term.c
+++ b/bogl-term.c
@@ -863,4 +863,5 @@ bogl_term_redraw (struct bogl_term *term)
     {
         show_cursor(term, 1);
     }        
+    bogl_update();
 }
diff --git a/bogl.c b/bogl.c
index f1486f9..5cbe96e 100644
--- a/bogl.c
+++ b/bogl.c
@@ -124,12 +124,12 @@ static void kbd_done (void);
 static void sigusr2 (int);
 
 static struct fb_fix_screeninfo fb_fix;
+static struct fb_var_screeninfo fb_var;
 /* Initialize BOGL. */
 int
 bogl_init (void)
 {
   unsigned long bogl_frame_offset, bogl_frame_len;
-  struct fb_var_screeninfo fb_var;
   struct vt_stat vts;
   int PAGE_MASK = ~(sysconf(_SC_PAGESIZE) - 1);
 
@@ -457,6 +457,23 @@ bogl_fb_set_palette (int c, int nc, const unsigned char palette[][3])
   ioctl (fb, FBIOPUTCMAP, &cmap);
 }
 
+/* Update screen, some framebuffer driver require this explicit notify after
+   modifying screen contents */
+void
+bogl_update (void)
+{
+#if BOGL_VGA16_FB
+  if (type == FB_TYPE_VGA_PLANES)
+    {
+      /* There is no need to call FBIOPAN_DISPLAY when using vga16fb driver.
+         What's worse, it may cause screen flicker on certain hardwares.
+	 So make bogl_update() a no-op here. */
+      return;
+    }
+#endif
+  ioctl (fb, FBIOPAN_DISPLAY, &fb_var);
+}
+
 /* Returns the oldest error message since this function was last
    called.  Clears the error state.  Returns a null pointer if no
    errors have occurred.  The caller must free the returned
diff --git a/bogl.h b/bogl.h
index 4d99788..61b0275 100644
--- a/bogl.h
+++ b/bogl.h
@@ -79,6 +79,7 @@ const char *bogl_error (void);
 
 void bogl_gray_scale (int make_gray);
 void bogl_fb_set_palette (int c, int nc, const unsigned char palette[][3]);
+void bogl_update(void);
 void bogl_rectangle (int x1, int y1, int x2, int y2, int c);
 int bogl_metrics (const char *s, int n, const struct bogl_font *font);
 
-- 
2.30.2

